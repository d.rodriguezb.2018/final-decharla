# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: David Rodríguez Bravo
* Titulación: Ingeniería en Tecnologías de la Telecomunicación
* Cuenta en laboratorios: darobra
* Cuenta URJC: d.rodriguezb.2018
* Video básico (url): https://youtu.be/2bzuQpqG-zI
* Video parte opcional (url): https://youtu.be/tzIucC0W9WM
* Despliegue (url): https://darobra.pythonanywhere.com/decharlas
* Contraseñas: pass_1, pass_2
* Cuenta Admin Site: darobra/1234

## Resumen parte obligatoria
En esta practica tenemos una vista inicial donde encontramos en la parte izquierda un menu desplegable con las diferentes opciones:
* Página principal: Pagina inicial, donde podemos ver las salas disponibles con los mensajes que tienen en total y los no leidos. Además del número de votos de casa sala. Podemos introducir un nuevo nombre de usuario y entrar en la sala que queramos.
* Configuración: Tenemos 3 opciones. Podemos cambiar el tamaño y fuente de la letra, ademas de cambiar el nombre del usuario.
* Ayuda: Zona donde nos explica el funcionamiento de la aplicación web.
* Página del admin.

Además, debajo de este menu desplegable encontramos el nombre de usuario (Anonymous_id si acabamos de entrar), y debajo tenemos la entrada a la sala donde escribimos el nombre de la sala que queremos crear. Pinchando en el botón Acceder a la sala, la creamos.
Una vez elegimos una sala y entramos, podemos ver los mensajes que han escrito otros usuarios, y puedes escribir tantos mensajes como quieras. Además hay una casilla al lado del boton Enviar (Imagen-Msg), la cual si la marcamos podemos introducir imagenes en el chat escribiendo la url de la imagen.
Finalmente, si creamos una sala con el mismo nombre de otra ya creada, entraremos en la sala con ese dicho nombre.		

## Lista partes opcionales 

* Nombre parte: Inclusión de un favicon del sitio. Es una imagen situada en la carpeta statics, que desde urls.py redireccion cada vez que se pide.
* Nombre parte: Permitir que las sesiones autenticadas se puedan terminar. Esto es, que haya una opción para “terminar la sesión”, de forma que al usarla, si se vuelve a acceder al sitio vuelva a recibirse el formulario para autenticarse. Al realizar este Logout se borra la cookie del Login provocando que tengamos que iniciar sesión obteniendo otra cookie diferente.
* Nombre parte: Permitir votar las las salas. Para ello, en cada sala aparecerá un botón “Me gusta” y otro de "Dislike" para indicar que se da un voto a la sala positivo o negativo. Cada sala, en el listado general, saldrá junto a los votos que tiene. Lo he implementado con un booleano donde Si-Megusta y No-Dislike. Ademas de tener un listado con el numero de Me Gustas y Dislikes de cada sala creada. Un mismo usuario solo puede realizar un voto.
